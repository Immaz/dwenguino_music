#include <Wire.h>

#include <Dwenguino.h>

#include <LiquidCrystal.h>

#include <Wire.h>

#include <Dwenguino.h>

#include <LiquidCrystal.h>

#include <DwenguinoMotor.h>

DCMotor dcMotor1(MOTOR_1_0, MOTOR_1_1);

#define _A3 (220.0)
#define _B3 (246.94)
#define _GS3 (207.65)
#define _A4 (440.0)
#define _B4 (493.88)
#define _C4 (261.63)
#define _D4 (293.66)
#define _E4 (329.63)
#define _GS4 (415.0)
#define _C5 (523.25)
#define _D5 (587.33)
#define _E5 (659.25)
#define _F5 (698.46)
#define _G5 (783.99)
#define _A5 (880.0)

#define _R (0)


float lead_notes[] = {
  _E5, _B4, _C5, _D5, _C5, _B4, _A4, _A4, _C5, _E5, _D5, _C5, _B4, _B4, _C5, _D5, _E5, _C5, _A4, _A4, _R,
  _D5, _F5, _A5, _G5, _F5, _E5, _C5, _E5, _D5, _C5, _B4, _B4, _C5, _D5, _E5, _C5, _A4, _A4, _R,

  _E4, _C4, _D4, _B3, _C4, _A3, _GS3, _B3, _E4, _C4, _D4, _B3, _C4, _E4, _A4, _A4, _GS4, _R
}; // 59 notes

float note_duration[] = {
  2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 1, 1, 2, 2, 2, 2, 2, 2,
  3, 1, 2, 1, 1, 3, 1, 2, 1, 1, 2, 1, 1, 2, 2, 2, 2, 2, 2,

  4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 2, 2, 6, 2
};
int last_note = NULL;


void setup()
{
  pinMode(BUZZER, OUTPUT);
  initDwenguino();
  dwenguinoLCD.setCursor(0, 0);
  dwenguinoLCD.print(String("Be ready for the theme"));
}

void loop()
{
  for (int i = 0; i < sizeof(lead_notes) / sizeof(float); i++) {
    float note = lead_notes[i];
    float duration = note_duration[i];
    dwenguinoLCD.clear();
    if (last_note != NULL) {
      dwenguinoLCD.setCursor(0, 1);
      dwenguinoLCD.print(String(last_note));
    }
    last_note = note;
    dwenguinoLCD.setCursor(0, 0);
    dwenguinoLCD.print(String(note));


    if (note == 0) {
      noTone(BUZZER);
    } else {
      tone(BUZZER, note);

      float note_distance_between_lights = 84.126;
      int amount_of_leds = (note - _GS3) / note_distance_between_lights;
      LEDS = pow(2, (amount_of_leds + 1)) - 1;
    }
    delay(duration * 1000 / 5);
  }
  noTone(BUZZER);

}

